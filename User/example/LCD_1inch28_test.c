#include "image.h"
#include "LCD_Test.h"
#include "LCD_1in28.h"
#include "DEV_Config.h"

#define GET_RGB565_R(per) (((uint16_t)(per/100.0*0x001F)) << 11)
#define GET_RGB565_G(per) (((uint16_t)(per/100.0*0x003F)) << 5)
#define GET_RGB565_B(per) (((uint16_t)(per/100.0*0x001F)) << 0)

const uint16_t color_arry[] = {
	WHITE, BLACK, BLUE, BRED, GRED,
	GBLUE, RED, MAGENTA, GREEN, CYAN,
	YELLOW,BROWN, BRRED, GRAY, DARKBLUE,
	LIGHTBLUE, GRAYBLUE, LIGHTGREEN, LGRAY,
	LGRAYBLUE, LBBLUE
};


void LCD_1in28_start()
{
	printf("LCD_1IN28_test Demo\r\n");
	DEV_Module_Init(); 
	printf("LCD_1IN28_ Init and Clear...\r\n");
	LCD_1IN28_SetBackLight(800);
	LCD_1IN28_Init(VERTICAL);

}

void LCD_1in28_test()
{
	LCD_1in28_start();
//	uint16_t color_num = sizeof(color_arry) / sizeof(uint16_t);
//	uint16_t i = 0;
//	
//	while (i < color_num)
//	{
//		LCD_1IN28_Clear(color_arry[i]);
//		i++;
//	}
	
	uint32_t color_full = 0;
	
	for(uint8_t r_per = 0; r_per < 100; r_per+=10)
	{
		for(uint8_t g_per = 0; g_per < 100; g_per+=10)
		{
			for(uint8_t b_per = 0; b_per < 100; b_per+=10)
			{
				color_full = GET_RGB565_R(r_per) + GET_RGB565_G(g_per) + GET_RGB565_B(b_per);
				LCD_1IN28_Clear(color_full);

			}
		}
	}
 	LCD_1IN28_Clear(BLACK);
	
	printf("Paint_NewImage\r\n");
	Paint_NewImage(LCD_1IN28_WIDTH,LCD_1IN28_HEIGHT, 0, BLACK);
	
	printf("Set Clear and Display Funtion\r\n");
	Paint_SetClearFuntion(LCD_1IN28_Clear);
	Paint_SetDisplayFuntion(LCD_1IN28_DrawPaint);

  printf("Paint_Clear\r\n");
	Paint_Clear(BLACK);
  DEV_Delay_ms(1000); 
	
	printf("drawing...\r\n");
	Paint_DrawCircle(120,120, 120, BLUE ,DOT_PIXEL_2X2,DRAW_FILL_EMPTY);
  Paint_DrawLine  (120, 0, 120, 12,GREEN ,DOT_PIXEL_4X4,LINE_STYLE_SOLID);
  Paint_DrawLine  (120, 228, 120, 240,GREEN ,DOT_PIXEL_4X4,LINE_STYLE_SOLID);
  Paint_DrawLine  (0, 120, 12, 120,GREEN ,DOT_PIXEL_4X4,LINE_STYLE_SOLID);
  Paint_DrawLine  (228, 120, 240, 120,GREEN ,DOT_PIXEL_4X4,LINE_STYLE_SOLID);
	
  Paint_DrawImage(gImage_70X70, 85, 25, 70, 70); 
  Paint_DrawString_CN(56,140, "΢ѩ����",   &Font24CN,BLACK,  WHITE);
  Paint_DrawString_EN(123, 123, "WAVESHARE",&Font16,  BLACK, GREEN); 
	
	Paint_DrawLine  (120, 120, 70, 70,YELLOW ,DOT_PIXEL_3X3,LINE_STYLE_SOLID);
	Paint_DrawLine  (120, 120, 176, 64,BLUE ,DOT_PIXEL_3X3,LINE_STYLE_SOLID);
	Paint_DrawLine  (120, 120, 120, 210,RED ,DOT_PIXEL_2X2,LINE_STYLE_SOLID);
	
	printf("quit...\r\n");
	//DEV_Module_Exit();
  
}

